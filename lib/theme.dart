import 'package:flutter/material.dart';

final ThemeData scoredTheme = ThemeData(
  primarySwatch: Colors.green,
  brightness: Brightness.light,
  accentColor: const Color(0xffff9800),
);

final ThemeData scoredThemeDark = ThemeData(
  primarySwatch: Colors.green,
  brightness: Brightness.dark,
  accentColor: const Color(0xffff9800),
);
