import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:scored/generated/i18n.dart';
import 'package:scored/notifiers/lang_notifier.dart';
import 'package:scored/notifiers/players_notifier.dart';
import 'package:scored/notifiers/theme_notifier.dart';
import 'package:scored/theme.dart';
import 'package:scored/ui/screens/score/score_page.dart';
import 'package:scored/ui/screens/setup/setup_page.dart';
import 'package:scored/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  SharedPreferences.getInstance().then((SharedPreferences prefs) {
    final bool darkMode = prefs.getBool(darkModeKey) ?? false;
    final Locale locale = _getLocale(prefs.getString(langKey));

    _setSystemNavBarColor(darkMode);

    runApp(
      MultiProvider(
        providers: <SingleChildCloneableWidget>[
          ChangeNotifierProvider<PlayersNotifier>(
            builder: (_) => PlayersNotifier(),
          ),
          ChangeNotifierProvider<ThemeNotifier>(
            builder: (_) => ThemeNotifier(darkMode),
          ),
          ChangeNotifierProvider<LangNotifier>(
            builder: (_) => LangNotifier(locale),
          ),
        ],
        child: MyApp(),
      ),
    );
  });
}

Locale _getLocale(String lang) {
  Locale locale = lang != null ? Locale(lang, '') : Locale(Platform.localeName);

  if (!S.delegate.supportedLocales.contains(locale)) {
    locale = const Locale('en');
  }

  return locale;
}

void _setSystemNavBarColor(bool darkMode) {
  final SystemUiOverlayStyle style =
      darkMode ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light;
  final Color systemNavColor = darkMode
      ? scoredThemeDark.primaryColorDark
      : scoredTheme.primaryColorDark;

  SystemChrome.setSystemUIOverlayStyle(
      style.copyWith(systemNavigationBarColor: systemNavColor));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer2<ThemeNotifier, LangNotifier>(
      builder: (_, ThemeNotifier themeNotifier, LangNotifier langNotifier, __) {
        return MaterialApp(
          locale: langNotifier.locale,
          localizationsDelegates: <LocalizationsDelegate<dynamic>>[
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          onGenerateTitle: (BuildContext context) => S.of(context).app_title,
          supportedLocales: S.delegate.supportedLocales,
          theme: themeNotifier.isDark ? scoredThemeDark : scoredTheme,
          home: SetupPage(),
          routes: <String, WidgetBuilder>{
            '/score': (BuildContext context) => ScorePage(),
          },
        );
      },
    );
  }
}
