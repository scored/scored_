import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:scored/generated/i18n.dart';
import 'package:scored/models/player.dart';
import 'package:scored/notifiers/players_notifier.dart';
import 'package:scored/ui/screens/score/widgets/score_tile.dart';

class ScorePage extends StatefulWidget {
  @override
  _ScoreState createState() => _ScoreState();
}

class _ScoreState extends State<ScorePage> {
  PlayersNotifier _playersNotifier;
  DateTime _backPressedTime;

  @override
  void initState() {
    super.initState();
    _playersNotifier = Provider.of<PlayersNotifier>(context, listen: false);
    SystemChrome.setEnabledSystemUIOverlays(<SystemUiOverlay>[]);
  }

  @override
  void dispose() {
    super.dispose();
    _playersNotifier.clearScores();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(),
      child: Scaffold(
        key: const Key('ScoredPage'),
        body: Consumer<PlayersNotifier>(builder:
            (BuildContext ctx, PlayersNotifier playersNotifier, Widget child) {
          return OrientationBuilder(
            builder: (BuildContext ctx2, Orientation orientation) {
              return _content(playersNotifier.list, orientation);
            },
          );
        }),
      ),
    );
  }

  Widget _content(List<Player> players, Orientation orientation) {
    final int count = players.length;

    if (count == 1) {
      return ScoreTile(
        onDecrement: () => _decrement(0),
        onIncrement: () => _increment(0),
        player: players[0],
      );
    }

    if (count <= 3) {
      return orientation == Orientation.portrait
          ? _singleColumnBody(players)
          : _singleRowBody(players);
    }

    return orientation == Orientation.portrait
        ? _twoColumnsBody(players)
        : _twoRowsBody(players);
  }

  void _decrement(int idx) {
    final Player player = _playersNotifier.list[idx]..score -= 1;

    _playersNotifier.set(player, idx);
  }

  void _increment(int idx) {
    final Player player = _playersNotifier.list[idx]..score += 1;

    _playersNotifier.set(player, idx);
  }

  Future<bool> _onBackPressed() {
    final DateTime now = DateTime.now();
    if (_backPressedTime == null ||
        now.difference(_backPressedTime) > Duration(seconds: 2)) {
      _backPressedTime = now;

      Fluttertoast.showToast(msg: S.of(context).leave_confirm);

      return Future<bool>.value(false);
    }
    return Future<bool>.value(true);
  }

  Column _singleColumnBody(List<Player> players) {
    final List<Widget> children = <Widget>[];

    final double height = MediaQuery.of(context).size.height / players.length;
    for (int i = 0; i < players.length; i++) {
      children.add(_scoreTile(players[i], i, height: height));
    }

    return Column(children: children);
  }

  Row _singleRowBody(List<Player> players) {
    final List<Widget> children = <Widget>[];

    final double width = MediaQuery.of(context).size.width / players.length;
    for (int i = 0; i < players.length; i++) {
      children.add(_scoreTile(players[i], i, width: width));
    }

    return Row(children: children);
  }

  Column _twoColumnsBody(List<Player> players) {
    final List<Widget> children = <Widget>[];
    final double width = MediaQuery.of(context).size.width / 2;
    final int count = players.length;

    int rows = count ~/ 2;
    double height = MediaQuery.of(context).size.height / rows;

    if (count.isOdd) {
      rows += 1;
      height = MediaQuery.of(context).size.height / rows;
      height += height / (1.5 * count);
    }

    for (int i = 0; i < players.length - 1; i += 2) {
      children.add(Row(
        children: <Widget>[
          _scoreTile(players[i], i, height: height, width: width),
          _scoreTile(players[i + 1], i + 1, height: height, width: width),
        ],
      ));
    }

    if (count.isOdd) {
      children.add(Row(
        children: <Widget>[
          _scoreTile(players.last, count - 1,
              height: MediaQuery.of(context).size.height - (rows - 1) * height,
              width: MediaQuery.of(context).size.width)
        ],
      ));
    }

    return Column(
      children: children,
    );
  }

  Row _twoRowsBody(List<Player> players) {
    final List<Widget> children = <Widget>[];
    final double height = MediaQuery.of(context).size.height / 2;
    final int count = players.length;

    int cols = count ~/ 2;
    double width = MediaQuery.of(context).size.width / cols;

    if (count.isOdd) {
      cols++;
      width = MediaQuery.of(context).size.width / cols;
      width += width / (1.5 * count);
    }

    for (int i = 1; i < players.length; i += 2) {
      children.add(Column(
        children: <Widget>[
          _scoreTile(players[i], i, height: height, width: width),
          _scoreTile(players[i - 1], i - 1, height: height, width: width),
        ],
      ));
    }

    if (count.isOdd) {
      children.add(Column(
        children: <Widget>[
          _scoreTile(players.last, count - 1,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width - (cols - 1) * width)
        ],
      ));
    }

    return Row(
      children: children,
    );
  }

  Container _scoreTile(Player player, int idx, {double height, double width}) {
    return Container(
      height: height,
      width: width,
      child: ScoreTile(
        onDecrement: () => _decrement(idx),
        onIncrement: () => _increment(idx),
        player: player,
      ),
    );
  }
}
