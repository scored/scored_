import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scored/generated/i18n.dart';
import 'package:scored/models/TextInputState.dart';
import 'package:scored/models/player.dart';
import 'package:scored/notifiers/players_notifier.dart';
import 'package:scored/ui/screens/setup/widgets/bottom_bar.dart';
import 'package:scored/ui/screens/setup/widgets/bottom_menu_sheet.dart';
import 'package:scored/ui/screens/setup/widgets/fab.dart';
import 'package:scored/ui/screens/setup/widgets/player_item.dart';
import 'package:scored/ui/widgets/custom_center_docked_fab_location.dart';

class SetupPage extends StatefulWidget {
  @override
  _SetupPageState createState() => _SetupPageState();
}

class _SetupPageState extends State<SetupPage> {
  List<TextInputState> _textInputsStates = <TextInputState>[TextInputState()];

  bool _addDisabled = false;
  PlayersNotifier _playersNotifier;

  @override
  void initState() {
    super.initState();
    _playersNotifier = Provider.of<PlayersNotifier>(context, listen: false);
  }

  @override
  void dispose() {
    for (int i = 0; i < _textInputsStates.length; i++) {
      _textInputsStates[i].controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: const Key('SetupPage'),
      appBar: AppBar(
        title: Text(S.of(context).app_title),
      ),
      body: SafeArea(
        child: Consumer<PlayersNotifier>(
          builder: (BuildContext context, PlayersNotifier playersNotifier,
                  Widget child) =>
              _playerList(playersNotifier.list),
        ),
      ),
      floatingActionButton: Fab(
        disabled: _addDisabled,
        onAdd: (bool added) => _addPlayer(added),
      ),
      bottomNavigationBar: BottomBar(
        onClearAll: _clearAll,
        onShowMore: _showMore,
        onStart: _startGame,
      ),
      floatingActionButtonLocation: CustomCenterDockedFabLocation(context),
    );
  }

  void _addPlayer(bool added) {
    if (added) {
      _textInputsStates.add(TextInputState());
    } else {
      setState(() {
        _addDisabled = true;
      });
    }
  }

  void _clearAll() {
    _playersNotifier.clear();
    setState(() {
      _textInputsStates = <TextInputState>[TextInputState()];
      _addDisabled = false;
    });
  }

  Widget _listItem(List<Player> players, int idx) {
    if (idx.isOdd) {
      return const Divider();
    }

    final int i = idx ~/ 2;

    final TextInputState state = _textInputsStates[i];
    final Player player = players[i];

    return PlayerItem(
      controller: state.controller,
      deletable: players.length > 1,
      focusNode: state.focusNode,
      last: i == players.length - 1,
      onChangeName: (String name) => _setName(i, name),
      onRemovePlayer: () => _removePlayerAt(i),
      onSelectColor: (Color color) => _setColor(i, color),
      onSubmitted: () => _submitName(i),
      player: player,
    );
  }

  ListView _playerList(List<Player> players) {
    return ListView.builder(
      itemBuilder: (BuildContext ctx, int idx) => _listItem(players, idx),
      itemCount: (players.length * 2) - 1,
      padding: const EdgeInsets.only(bottom: 48),
    );
  }

  void _removePlayerAt(int idx) {
    _playersNotifier.remove(idx);
    setState(() {
      _addDisabled = false;
      _textInputsStates.removeAt(idx);
    });
  }

  void _setColor(int idx, Color color) {
    final Player player = _playersNotifier.list[idx]..color = color;
    _playersNotifier.set(player, idx);
    Navigator.of(context).pop();
  }

  void _setName(int idx, String name) {
    final Player player = _playersNotifier.list[idx]..name = name;
    _playersNotifier.set(player, idx);
  }

  void _showMore() {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) => BottomMenuSheet(),
    );
  }

  void _startGame() {
    Navigator.of(context).pushNamed('/score');
  }

  void _submitName(int index) {
    if (index < _textInputsStates.length - 1) {
      FocusScope.of(context)
          .requestFocus(_textInputsStates[index + 1].focusNode);
    }
  }
}
