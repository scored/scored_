import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void showAbout(BuildContext context) {
  final Widget icon = Image.asset('assets/icon.png', width: 48, height: 48);
  final TextStyle bodyStyle = Theme.of(context).textTheme.body1;
  final TextStyle linkStyle = TextStyle(
    color: Theme.of(context).accentColor,
    decorationStyle: TextDecorationStyle.solid,
  );

  showAboutDialog(
    context: context,
    applicationVersion: '1.0.0-beta3',
    applicationIcon: icon,
    applicationLegalese: '© 2019 Matteo Taroli',
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top: 24),
        child: RichText(
            text: TextSpan(
          children: <TextSpan>[
            TextSpan(
              style: bodyStyle,
              text:
                  'Scored! is an open-source application made with Flutter by ',
            ),
            _LinkTextSpan(
              text: 'Matteo Taroli',
              style: linkStyle,
              url: 'https://tteo.be',
            ),
            TextSpan(
              style: bodyStyle,
              text: '.\n\nThe source code can be found in the ',
            ),
            _LinkTextSpan(
              text: 'Scored! Gitlab repository',
              style: linkStyle,
              url: 'https://gitlab.com/scored/scored_',
            ),
            TextSpan(
              style: bodyStyle,
              text: '.',
            ),
          ],
        )),
      ),
    ],
  );
}

// cf https://github.com/flutter/flutter/blob/master/examples/flutter_gallery/lib/gallery/about.dart
class _LinkTextSpan extends TextSpan {
  _LinkTextSpan({TextStyle style, String url, String text})
      : super(
            style: style,
            text: text ?? url,
            recognizer: TapGestureRecognizer()
              ..onTap = () => launch(url, forceWebView: false));
}
