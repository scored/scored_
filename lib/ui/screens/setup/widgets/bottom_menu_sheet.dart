import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scored/generated/i18n.dart';
import 'package:scored/notifiers/lang_notifier.dart';
import 'package:scored/notifiers/theme_notifier.dart';
import 'package:scored/ui/screens/setup/widgets/about_dialog.dart';
import 'package:scored/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BottomMenuSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeNotifier themeNotifier =
        Provider.of<ThemeNotifier>(context, listen: false);

    final LangNotifier langNotifier =
        Provider.of<LangNotifier>(context, listen: false);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CheckboxListTile(
          title: Text(S.of(context).bottom_sheet_dark_mode),
          value: themeNotifier.isDark,
          onChanged: (bool value) => _setTheme(value, themeNotifier),
        ),
        ListTile(
          title: Text(S.of(context).language_label),
          trailing: DropdownButton<String>(
            items: S.delegate.supportedLocales.map((Locale locale) {
              return DropdownMenuItem<String>(
                value: locale.languageCode,
                child: Text(locale.languageCode),
              );
            }).toList(),
            value: langNotifier.locale.languageCode,
            onChanged: (String lang) => _setLang(lang, langNotifier),
          ),
        ),
        ListTile(
          title: Text(S.of(context).bottom_sheet_about),
          onTap: () => showAbout(context),
        ),
      ],
    );
  }

  void _setTheme(bool value, ThemeNotifier themeNotifier) {
    themeNotifier.isDark = value;
    SharedPreferences.getInstance()
        .then((SharedPreferences prefs) => prefs.setBool(darkModeKey, value));
  }

  void _setLang(String lang, LangNotifier langNotifier) {
    langNotifier.locale = Locale(lang);
    SharedPreferences.getInstance()
        .then((SharedPreferences prefs) => prefs.setString(langKey, lang));
  }
}
