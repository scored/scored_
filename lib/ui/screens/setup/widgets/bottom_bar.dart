import 'package:flutter/material.dart';
import 'package:scored/generated/i18n.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({this.onShowMore, this.onClearAll, this.onStart});

  final VoidCallback onShowMore;
  final VoidCallback onClearAll;
  final VoidCallback onStart;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      notchMargin: 4,
      color: Theme.of(context).primaryColor,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            icon: Icon(
              Icons.more_vert,
              color: Colors.white,
            ),
            tooltip: S.of(context).bottom_bar_more,
            onPressed: onShowMore,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.clear_all,
                  color: Colors.white,
                ),
                tooltip: S.of(context).bottom_bar_clear,
                onPressed: onClearAll,
              ),
              IconButton(
                icon: Icon(
                  Icons.play_arrow,
                  color: Colors.white,
                ),
                tooltip: S.of(context).bottom_bar_start,
                onPressed: onStart,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
