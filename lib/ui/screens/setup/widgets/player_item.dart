import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:scored/generated/i18n.dart';
import 'package:scored/models/player.dart';

class PlayerItem extends StatelessWidget {
  const PlayerItem({
    @required this.controller,
    this.deletable = false,
    @required this.focusNode,
    this.last = false,
    @required this.onChangeName,
    @required this.onRemovePlayer,
    @required this.onSelectColor,
    @required this.onSubmitted,
    @required this.player,
  });

  final TextEditingController controller;
  final bool deletable;
  final FocusNode focusNode;
  final bool last;
  final ValueChanged<String> onChangeName;
  final VoidCallback onRemovePlayer;
  final ValueChanged<Color> onSelectColor;
  final VoidCallback onSubmitted;
  final Player player;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: GestureDetector(
        child: CircleColor(
          color: player.color,
          circleSize: 48,
        ),
        onTap: () => _openColorPicker(context),
      ),
      title: TextField(
        decoration: InputDecoration(
          hintText: S.of(context).player_item_input_hint,
          labelText: S.of(context).player_item_input_label,
        ),
        controller: controller,
        cursorColor: player.color,
        focusNode: focusNode,
        onChanged: (String value) => onChangeName(value),
        textCapitalization: TextCapitalization.words,
        textInputAction: last ? TextInputAction.done : TextInputAction.next,
        onSubmitted: (_) => onSubmitted(),
      ),
      trailing: deletable
          ? IconButton(
              icon: Icon(
                Icons.delete,
                color: Colors.red[700],
              ),
              tooltip: S.of(context).player_item_delete(player.name),
              onPressed: () => onRemovePlayer(),
            )
          : null,
    );
  }

  void _openColorPicker(BuildContext context) {
    showDialog<void>(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(S.of(context).player_item_color_picker_title),
          content: MaterialColorPicker(
            onlyShadeSelection: true,
            onColorChange: (Color color) {
              onSelectColor(color);
            },
            selectedColor: player.color,
            shrinkWrap: true,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(S.of(context).common_cancel),
              onPressed: () => onSelectColor(player.color),
            )
          ],
        );
      },
    );
  }
}
