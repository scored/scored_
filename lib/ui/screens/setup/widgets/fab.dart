import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scored/generated/i18n.dart';
import 'package:scored/models/player.dart';
import 'package:scored/notifiers/players_notifier.dart';
import 'package:scored/ui/utils.dart';

class Fab extends StatelessWidget {
  const Fab({this.disabled, this.onAdd});

  final bool disabled;
  final ValueChanged<bool> onAdd;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: disabled ? Colors.grey : Theme.of(context).accentColor,
      onPressed: disabled ? null : () => onAdd(_addPlayer(context)),
      tooltip: S.of(context).fab_add_tooltip,
      child: const Icon(Icons.add),
    );
  }

  bool _addPlayer(BuildContext context) {
    final PlayersNotifier playersNotifier =
        Provider.of<PlayersNotifier>(context, listen: false);

    if (playersNotifier.list.length < PlayersNotifier.maxPlayers) {
      playersNotifier.add(Player());
      return true;
    }

    showSnackBar(context,
        S.of(context).fab_add_max(PlayersNotifier.maxPlayers.toString()));
    return false;
  }
}
