import 'package:flutter/material.dart';

void showSnackBar(BuildContext ctx, String text) {
  final SnackBar snackBar = SnackBar(
    content: Text(text),
  );
  Scaffold.of(ctx).showSnackBar(snackBar);
}
