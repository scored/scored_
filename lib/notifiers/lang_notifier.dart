import 'package:flutter/material.dart';

class LangNotifier extends ChangeNotifier {
  Locale _locale;

  LangNotifier(Locale locale) {
    _locale = locale;
  }

  Locale get locale => _locale;

  set locale(Locale locale) {
    _locale = locale;

    notifyListeners();
  }
}
