import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:scored/models/player.dart';

class PlayersNotifier extends ChangeNotifier {
  static const int maxPlayers = 15;
  static const int minPlayers = 1;

  List<Player> _players = <Player>[Player()];

  UnmodifiableListView<Player> get list =>
      UnmodifiableListView<Player>(_players);

  void add(Player player) {
    if (_players.length == maxPlayers) {
      return;
    }
    _players.add(player);
    notifyListeners();
  }

  void clear() {
    _players = <Player>[Player()];
    notifyListeners();
  }

  void clearScores() {
    for (Player player in _players) {
      player.score = 0;
    }
    notifyListeners();
  }

  void set(Player player, int idx) {
    _players[idx] = player;
    notifyListeners();
  }

  void remove(int idx) {
    if (_players.length == minPlayers) {
      return;
    }
    _players.removeAt(idx);
    notifyListeners();
  }
}
