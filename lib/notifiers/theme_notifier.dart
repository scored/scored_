import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:scored/theme.dart';

class ThemeNotifier extends ChangeNotifier {
  bool _isDark;

  ThemeNotifier(bool isDark) {
    _isDark = isDark;
  }

  bool get isDark => _isDark;

  set isDark(bool isDark) {
    _isDark = isDark;

    _setSystemNavBarColor().then<void>((_) => notifyListeners());
  }

  Future<void> _setSystemNavBarColor() async {
    final Color color = _isDark
        ? scoredThemeDark.primaryColorDark
        : scoredTheme.primaryColorDark;

    await FlutterStatusbarcolor.setNavigationBarColor(color);

    if (useWhiteForeground(color)) {
      FlutterStatusbarcolor.setNavigationBarWhiteForeground(true);
    } else {
      FlutterStatusbarcolor.setNavigationBarWhiteForeground(false);
    }
  }
}
