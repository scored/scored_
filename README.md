<div align="center">
	<img src="./icon.png"/>
</div>


__/!\ This repository only contains the code for the v1 of Scored! which won't be updated anymore. For the latest version of Scored!, please go to the new [Scored! repository](https://gitlab.com/scored/scored.gitlab.io) /!\\__

# Scored
Scored is a mobile app made with [flutter](https://flutter.dev/).

It allows to keep track of people's score or anything else 👌

## Get the app
The app can be found on the [Google Play Store](https://play.google.com/store/apps/details?id=be.tteo.scored)
